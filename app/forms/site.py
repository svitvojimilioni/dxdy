from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired

class SiteForm(FlaskForm):
    name = StringField('Naziv', validators=[DataRequired()])
    base_url = StringField('Link', validators=[DataRequired()])
    feed_url = StringField('RSS', validators=[DataRequired()])
    submit_url = SubmitField("Dodaj sajt")
