from flask import render_template, redirect

from . import app, db
from .models.site import Site
from .forms.site import SiteForm

def get_basic_data():
    data = {}
    data["APP_NAME"] = app.config["APP_NAME"]
    return data

@app.route("/", methods=["GET"])
def index():
    data = get_basic_data()
    data["sites"] = Site.query.all()
    return render_template("pages/index.html", data=data)

@app.route("/add", methods=["GET", "POST"])
def submit_site():
    data = get_basic_data()
    form = SiteForm()
    if form.validate_on_submit():
        name = form.name.data
        base_url = form.base_url.data
        feed_url = form.feed_url.data
        site = Site(name=name, base_url=base_url, feed_url=feed_url)

        db.session.add(site)
        db.session.commit()
        return redirect("/")
    data["form"] = SiteForm()
    print(dir(data["form"]))
    return render_template("pages/submit_site.html", data=data, form=data["form"])
