from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)

config_string = os.environ.get("FLASK_CONFIG", "config.DevelopmentConfig")
app.config.from_object("config.DevelopmentConifg")
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///app.db"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

from . import routes

