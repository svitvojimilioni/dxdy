from app import db

class Article(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(2000), unique=True)
    title = db.Column(db.String(2000))
    raw_content = db.Column(db.Text)
    debloated_content = db.Column(db.Text)
    summarized_content = db.Column(db.Text)

    site_id = db.Column(db.Integer, db.ForeignKey('site.id'))
