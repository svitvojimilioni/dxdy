from app import db
from app.models.article import Article

class Site(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    base_url = db.Column(db.String(1000), unique=True)
    feed_url = db.Column(db.String(1000), unique=True)
    articles = db.relationship('Article', backref='site')
    # TODO add creation date
    # TODO add last_updated date
